mkdir -p combined/
mkdir -p combined/items/

rm -rf combined/*.html
rm -rf combined/items/*.html

cat template/header.html $(ls -r items/*.html) template/footer.html > combined/index.html
cp items/*.html combined/items/
